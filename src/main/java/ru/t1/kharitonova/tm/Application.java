package ru.t1.kharitonova.tm;

import ru.t1.kharitonova.tm.constant.ArgumentConst;

import ru.t1.kharitonova.tm.constant.CommandConst;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        runArguments(args);
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:\n");
            final String command = scanner.nextLine();
            runCommand(command);
        }
    }

    private static void runArguments(final String[] arg){
        if(arg == null || arg.length == 0) return;
        for (int i = 0; i < arg.length; i++) {
            runArgument(arg[i]);
        }
        System.exit(0);
    }

    private static void runCommand(final String param){
        switch (param) {
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showCommandError();
        }
    }

    private static void runArgument(final String param){
        switch (param) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showArgumentError();
        }
    }

    private static void showWelcome(){
        System.out.println("** WELCOME TO TASK MANAGER**");
    }

    private static void showAbout(){
        System.out.println("[ABOUT]");
        System.out.println("name: Anastasiya Kharitonova");
        System.out.println("e-mail: akharitonova@t1-consulting.ru");
    }

    private static void showVersion(){
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    private static void showHelp(){
        System.out.println("[HELP]");
        System.out.printf("%s, %s : Display developer info. \n", CommandConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s : Display program version. \n", CommandConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s : Display list of terminal commands. \n", CommandConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s : Close application. \n", CommandConst.EXIT);
    }

    private static void showArgumentError(){
        System.out.println("[ERROR]");
        System.out.println("This argument is not supported");
        System.exit(1);
    }

    private static void showCommandError(){
        System.out.println("[ERROR]");
        System.out.println("This command is not supported");
        System.exit(1);
    }

    public static void exit() {
        System.exit(0);
    }

}
